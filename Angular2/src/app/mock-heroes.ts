import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Mr. Nice', type: 'TEQ' },
    { id: 12, name: 'Narco', type: 'AGL' },
    { id: 13, name: 'Bombasto', type: 'STR' },
    { id: 14, name: 'Celeritas', type: 'STR' },
    { id: 15, name: 'Magneta', type: 'STR' },
    { id: 16, name: 'RubberMan', type: 'TEQ' },
    { id: 17, name: 'Dynama', type: 'AGL' },
    { id: 18, name: 'Dr IQ', type: 'INT' },
    { id: 19, name: 'Magma', type: 'TEQ' },
    { id: 20, name: 'Tornado', type: 'STR' }
]